{
  inputs = {
    utils.url = "github:numtide/flake-utils/main";
  };

  outputs = { self, nixpkgs, utils }:
    utils.lib.eachDefaultSystem(system:
      let pkgs = import nixpkgs { inherit system; }; in {
        devShells.default = pkgs.mkShell {
          buildInputs = with pkgs; [
            # Command aliases for managing website
            (pkgs.writeShellScriptBin "build" "sass source:public/assets/css -s compressed")
            (pkgs.writeShellScriptBin "clean" "rm -rf public/assets/css .wrangler")
            (pkgs.writeShellScriptBin "deploy" "wrangler pages deploy")
            (pkgs.writeShellScriptBin "format" "prettier -w $PWD")

            # Tools needed for building
            nodePackages.prettier
            nodePackages.sass
            wrangler
          ];
        };
      });
}
