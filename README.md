Website
![license](https://img.shields.io/badge/license-0BSD-blue?style=flat-square)
![activity](https://img.shields.io/gitea/last-commit/hanna/website?gitea_url=https%3A%2F%2Fcodeberg.org&style=flat-square)
================================================================================

This repository contains the source code for my personal website.

## Building for Production

To build my website I recommend using [nix](https://nixos.org/) as it will
download the necessary tools.

```console
$ nix develop -c "build"
$ nix develop -c "deploy"
🌏  Uploading... (8/8)

✨ Success! Uploaded 2 files (6 already uploaded) (1.08 sec)

🌎 Deploying...
✨ Deployment complete!
```

Running the `just deploy` commmand will build and deploy the website to
cloudflare.
